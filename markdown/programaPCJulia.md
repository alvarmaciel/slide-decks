---
author: Alvar Maciel
title: Ideas del Curso
date: August 5, 2018
revealjs-url: "reveal.js"
theme: isovera
css:
  - "https://fonts.googleapis.com/css?family=Roboto+Slab:700"
---

# Índice

1.  [Como vamos a trabajar](#org42e8ed3)
2.  [Como es una clases:](#orgb43754b)
3.  [Herramientas](#orgefa03b4)

<a id="org42e8ed3"></a>

# Como vamos a trabajar

Vamos a usar dos canales de comunicación:

- La video llamada o Audio llamada
  - Meet/Jitsi y en el mejor de los casos Discord
- El mail
  - alvarmaciel@gmail

<a id="orgb43754b"></a>

# Como es una clases:
<div style="display: inline-block; width: 40%">
![Programming](https://media.giphy.com/media/l3vRmVv5P01I5NDAA/giphy.gif)\ 
</div>
<div style="display: inline-block; width: 60">
- Revisión de lo que vimos.
- Ejercitación.
- Proyecto de programación.
- Análisis de lo que hicimos.
- Tarea para no olvidarnos
</div>
<a id="orgefa03b4"></a>

# Herramientas

- Cuenta en [Scratch](https://scratch.mit.edu)
- [Página de Pilas Bloques](http://pilasbloques.program.ar/)
- Mailing
- Algún lugar para tomar notas, si es cuaderno, que sea cuadriculado. Si es en la compu pueden instalarle [Atom](https://atom.io/) y aprendemos a tomar notas. O el [Pad](https://pad.riseup.net/p/PCJulia-keep)
